import socket, select, string, sys, hashlib
from termcolor import *
lock_receive = False
def verifying():
	token_file = open('./client.py','r',encoding="utf-8")
	token_no_hashed = token_file.read()
	token = hashlib.sha256(str(token_no_hashed).encode('utf-8')).hexdigest()
	token_file.close()
	token_file = open('./token.key','w',encoding="utf-8")
	token_file.write(token)

	return token
token = verifying()
receiver = "all"
salon = "*"
commands = {
			"join": 1,
			"joinmdp": 2,
			"setmdp": 1,
			"lock": 1,
			"pv": 1,
			"unlock": 1	
}


def command_parser(msg_parser,commands):
	del msg_parser[0]
	if "/" in msg_parser[0]:
		
		command = msg_parser[0]
		command = command.replace("/", "")
		command = command.replace("\n", "")

		liste_parser = msg_parser
		del msg_parser[0]
		nbr_args = len(msg_parser)
		if commands[command]:
			if commands[command] == nbr_args:
				if len(msg_parser) == nbr_args:
					if command == "join":
						if msg_parser[0] == " ":
							pass
						else:
						 
							result = " #salon# " + msg_parser[0]	
							saloun = result
							saloun = saloun.replace(" #salon# ", "")
							saloun = saloun.replace("\n", "")
							print('Connected to {}'.format(saloun))
							return result
					if command == "joinmdp":
					
						
						result = " #salon# " + msg_parser[0] + " #mdp# "+msg_parser[1]
						saloun = result
						saloun = saloun.replace(" #salon# ", "")
						saloun = saloun.replace("\n", "")
						print('Connected to {}'.format(saloun))
						return result
					if command == "lock":
						pseudo = msg_parser[0]
						receiver = "#pseudol#"+pseudo
						return receiver
					if command == "unlock":
						tock = "#unlock#"
						return tock
							



	

def prompt() :
	sys.stdout.write('<'+name+'> ')
	sys.stdout.flush()
 
#main function
if __name__ == "__main__":
 
	if(len(sys.argv) < 3) :
		print('Usage : python client.py hostname port')
		sys.exit()
	def namem():
		name = input('Name:')
		return name
	name = namem()

	if name== "ROOT":
		print("You aren't a superuser ")
		sys.exit()
 
	host = sys.argv[1]
	port = int(sys.argv[2])
 
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	
	s.settimeout(2)
 
	# connect to remote host
	try :
		s.connect((host, port))
	except :
		print('Unable to connect')
		sys.exit()
 
	print('Connected to remote host. Start sending messages')
	prompt()
 
	while 1:
		rlist = [sys.stdin, s]
 
		# Get the list sockets which are readable
		read_list, write_list, error_list = select.select(rlist , [], [])
 
		for sock in read_list:
			#incoming message from remote server
			if sock == s:
				data = sock.recv(4096).decode()
				if not data :
					print('\nDisconnected from chat server')
					sys.exit()
				else :
					#print data
					data_list = data.split(" ")
					if "#" in data_list[1]:
						msg_l = (data.split(' '))
						del msg_l[1]
						msg = " ".join(msg_l)
						name_in_msg = (msg.split('>'))
						name_in_msg = name_in_msg[0].replace("<", "")
						name_in_msg = name_in_msg[0].replace(">", "")
						
						sys.stdout.write('\nNEW SALON CREATE\n')
						if name_in_msg == name:
							salon = data_list[1].replace("#", "")
							salon = salon.replace("\n", "")
							print(salon)

						prompt()
					else:
						if (data.split(' '))[1] == salon:

							if (data.split(' '))[2] == "all" or (data.split(' '))[2] == name:
								msg_l = (data.split(' '))
								del msg_l[1]
								del msg_l[1]
								del msg_l[1]
								msg = " ".join(msg_l)

								name_in_msg = msg.split('>')

								sys.stdout.write(colored(msg+"\r",'cyan', attrs=['bold']))
								prompt()
						else:
							pass
 
			#user entered a message
			else :
				msg = sys.stdin.readline()

				msg_send = name+" "+msg
				res = command_parser(msg_send.split(" "),commands)
				if res:
					if "#salon#" in res:	
						salon = res		
						salon = salon.replace(" #salon# ", "")
						salon = salon.replace("\n", "")	
						msgname = (salon+" "+"SERVER"+" "+"all"+" "+"test"+" "+"A person entered on this salon\n").encode()
					if "#pseudol#" in res:
						receiver = res
						receiver = receiver.replace("#pseudol#", "")
						receiver = receiver.replace("\n", "")
			
						msgname = (salon+" "+"SERVER"+" "+receiver+" "+"test"+" "+name+" are locked on you.\n").encode()
					if "#unlock#" in res:
						
						receiver = "all"
						
						


				

				else:
					msgname = (salon+" "+name+" "+receiver+" "+token+" "+msg).encode()
				
					

				


	   
				s.send(msgname)
				prompt()