import socket, select, sys

#Function to broadcast chat messages to all connected clients
def broadcast_data (sock, message, liste):
    #Do not send the message to master socket and the client who has send us the message
    for socket in liste:
        if socket != server_socket and socket != sock :
            try :
              
                socket.send(message.encode())

            except :
                # broken socket connection may be, chat client pressed ctrl+c for example
                socket.close()
                CONNECTION_LIST.remove(socket)
 
if __name__ == "__main__":
    tokens = ['e55df3510f2f3b20f96d6efe4df60dafe3ed8123221bc87c980549d9a506fd90','e55df3510f2f3b20f96d6efe4df60dafe3ed8123221bc87c980549d9a506fd90']
    if(len(sys.argv) < 2) :
        print('Usage : python host.py port')
        sys.exit()
 
    port = int(sys.argv[1])
    # List to keep porttrack of socket descriptors
    CONNECTION_LIST = []
    RECV_BUFFER = 4096 # Advisable to keep it as an exponent of 2
    PORT = port
 
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # this has no effect, why ?
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind(("0.0.0.0", PORT))
    server_socket.listen(10)
 
    # Add server socket to the list of readable connections
    
    CONNECTION_LIST.append(server_socket)
 
    print("Chat server started on port " + str(PORT))
 
    while 1:
        # Get the list sockets which are ready to be read through select
        read_sockets,write_sockets,error_sockets = select.select(CONNECTION_LIST,[],[])
 
        for sock in read_sockets:
            #New connection
            if sock == server_socket:
                # Handle the case in which there is a new connection recieved through server_socket
                sockfd, addr = server_socket.accept()
                print(sockfd)
                CONNECTION_LIST.append(sockfd)
                '''print("Client (%s, %s) connected" % addr)'''
                print(addr)
                broadcast_data(sockfd, "A person entered room\n",CONNECTION_LIST)
 
            #Some incoming message from a client
            else:
                # Data recieved from client, process it
                try:
                    #In Windows, sometimes when a TCP program closes abruptly,
                    # a "Connection reset by peer" exception will be thrown
                    
                    data = sock.recv(RECV_BUFFER).decode()
                    root_inf = data.split(" ")
                    print(root_inf)
                    name = root_inf[1]
                    token = root_inf[3]
                    del root_inf[1] 
                    msg = " ".join(root_inf)
                    msg_list = msg.split(" ")

           



                        



                    
              
                    if data:
                        if token == tokens[0] or token == tokens[1]:

                            broadcast_data(sock, "\r" + "<"+name+"> "+msg,CONNECTION_LIST)
                            print("\r" + "<"+name+"> "+msg)
                        else: 
                            print('test')
 
                except:
                    broadcast_data(sock, "Client (%s, %s) is offline\n" % addr,CONNECTION_LIST)
                    print("Client (%s, %s) is offline" % addr)
                    sock.close()
                    try:
                        CONNECTION_LIST.remove(sock)
                    except:
                        
                        continue
                    continue
 
    server_socket.close()