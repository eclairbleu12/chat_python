import socket, select, string, sys
from termcolor import *

salon = "*"
commands = {
			"join": 1
}


def command_parser(msg_parser,commands):

	del msg_parser[0]
	if "/" in msg_parser[0]:
		print('COMMAND DETECTED')
		command = msg_parser[0]
		command = command.replace("/", "")

		liste_parser = msg_parser
		del msg_parser[0]
		nbr_args = len(msg_parser)
		if commands[command]:
			if commands[command] == nbr_args:
				if len(msg_parser) == nbr_args:
					if command == "join":
					
						
						result = " #salon# " + msg_parser[0]
						saloun = result
						saloun = saloun.replace(" #salon# ", "")
						saloun = saloun.replace("\n", "")
						print('Connected to {}'.format(saloun))
						return result
							


def namem():
	name = input('Name:')
	return name
name = namem()
if name== "ROOT":
	print("You aren't a superuser ")
	sys.exit()
	

def prompt() :
	sys.stdout.write('<'+name+'> ')
	sys.stdout.flush()
 
#main function
if __name__ == "__main__":
 
	if(len(sys.argv) < 3) :
		print('Usage : python client.py hostname port')
		sys.exit()
 
	host = sys.argv[1]
	port = int(sys.argv[2])
 
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	
	s.settimeout(2)
 
	# connect to remote host
	try :
		s.connect((host, port))
	except :
		print('Unable to connect')
		sys.exit()
 
	print('Connected to remote host. Start sending messages')
	prompt()
 
	while 1:
		rlist = [sys.stdin, s]
 
		# Get the list sockets which are readable
		read_list, write_list, error_list = select.select(rlist , [], [])
 
		for sock in read_list:
			#incoming message from remote server
			if sock == s:
				data = sock.recv(4096).decode()
				if not data :
					print('\nDisconnected from chat server')
					sys.exit()
				else :
					#print data
					data_list = data.split(" ")
					if "#" in data_list[1]:
						msg_l = (data.split(' '))
						del msg_l[1]
						msg = " ".join(msg_l)
						name_in_msg = (msg.split('>'))
						name_in_msg = name_in_msg[0].replace("<", "")
						name_in_msg = name_in_msg[0].replace(">", "")
						
						sys.stdout.write('\nNEW SALON CREATE\n')
						if name_in_msg == name:
							salon = data_list[1].replace("#", "")
							salon = salon.replace("\n", "")
							print(salon)

						prompt()
					else:

						if (data.split(' '))[1] == salon:
							msg_l = (data.split(' '))
							del msg_l[1]
							msg = " ".join(msg_l)
							name_in_msg = msg.split('>')

							sys.stdout.write(colored(msg+"\r",'cyan', attrs=['bold']))
							prompt()
						else:
							pass
 
			#user entered a message
			else :
				msg = sys.stdin.readline()

				msg_send = name+" "+msg
				res = command_parser(msg_send.split(" "),commands)
				if res:
					if "#salon#" in res:	
						salon = res		
						salon = salon.replace(" #salon# ", "")
						salon = salon.replace("\n", "")	
						msgname = (salon+" "+"SERVER"+" "+"A person entered on this salon").encode()
				

				else:
					msgname = (salon+" "+name+" "+msg).encode()
				


	   
				s.send(msgname)
				prompt()